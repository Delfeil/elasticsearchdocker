FROM balenalib/raspberry-pi:latest
## Install JDK:
RUN apt-get update && apt-get install -y \
    wget \
    openjdk-11-jre

# Set $JAVA_HOME
ENV JAVA_HOME /usr/lib/jvm/java-11-openjdk-armhf

# Install elasticsearch

RUN wget https://artifacts.elastic.co/downloads/elasticsearch/elasticsearch-7.5.2-no-jdk-linux-x86_64.tar.gz && mkdir elasticsearch && tar -xvzf elasticsearch-7.5.2-no-jdk-linux-x86_64.tar.gz -C elasticsearch --strip-components 1

# RUN tar -xvzf elasticsearch-7.5.2-no-jdk-linux-x86_64.tar.gz

RUN mkdir -p /usr/share/elasticsearch && mkdir -p /media/usb/ && mv elasticsearch/* /usr/share/elasticsearch && chmod +x /usr/share/elasticsearch/bin/elasticsearch

# COPY ./elasticsearch/ /usr/share/elasticsearch

COPY ./config/elasticsearch.yml /usr/share/elasticsearch/config/elasticsearch.yml

ENV PATH /usr/share/elasticsearch/bin:$PATH
EXPOSE 9200 9300

CMD ["elasticsearch"]